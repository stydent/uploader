package middleware

import (
	"log"
	"uploader/api"

	jwt_lib "github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

func PrepareError(c *gin.Context) {
	c.Next()

	if len(c.Errors) > 0 {
		publicErr := c.Errors.ByType(gin.ErrorTypePublic).Last()

		if publicErr != nil && publicErr.Meta != nil {
			c.JSON(publicErr.Meta.(*api.AppError).Code, publicErr.Meta.(*api.AppError))
		}

		// TODO: добавить логер для приватных
		// Пока просто все ошибки в консоль
		log.Printf("Ошибки: %v\n", c.Errors.Errors())
	}
}

func JSON(c *gin.Context) {
	c.Next()
	if data, ok := c.Get("data"); ok {
		code := 200
		if cd, ok := c.Get("code"); ok {
			code = cd.(int)
		}
		c.JSON(code, data)
	}
}

func AuthMiddleware(SecretKey string, db gorm.DB) gin.HandlerFunc {
	return func(c *gin.Context) {
		tokenStr := c.Request.Header.Get("X-Auth")
		token, err := jwt_lib.Parse(tokenStr, func(token *jwt_lib.Token) (interface{}, error) {
			return []byte(SecretKey), nil
		})
		switch err.(type) {
		case nil:
			if !token.Valid {
				c.Set("code", 401)
				c.Set("data", "invalid token")
				c.Abort()
			}
			idUser := int(token.Claims["id_user"].(float64))
			var user api.User
			if db.First(&user, idUser).RecordNotFound() {
				c.Set("code", 401)
				c.Set("data", "access denied")
				c.Abort()
			}
			c.Set("user", &user)
			var role api.Role
			db.Model(&user).Related(&role)
			db.Model(&role).Association("Permissions").Find(&role.Permissions)
			c.Set("role", &role)
			c.Next()
		case *jwt_lib.ValidationError:
			validationError := err.(*jwt_lib.ValidationError)

			switch validationError.Errors {
			case jwt_lib.ValidationErrorExpired:
				c.Set("code", 401)
				c.Set("data", "token is old")
				c.Abort()
			default:
				c.Set("code", 401)
				c.Set("data", "invalid token")
				c.Abort()
			}
		default:
			c.Set("code", 401)
			c.Set("data", "invalid token")
			c.Abort()
		}
	}
}

func CORS() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, X-Auth, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT")
		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}
		c.Next()
	}
}
