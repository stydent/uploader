package api

import (
	"fmt"
	"io"
	"math/rand"
	"os"
	"path/filepath"
	"strconv"
	"time"

	"github.com/jinzhu/gorm"
)

type Build struct {
	ID        int       `json:"id"`
	FileName  string    `json:"file"`
	Path      string    `json:"-"`
	UpdateID  int       `sql:"index" json:"-"`
	CreatedAt time.Time `json:"created_at"`
	OsID      int       `sql:"index" json:"-"`
	Os        OS        `sql:"-" json:"system"`
}

type OS struct {
	ID   int    `json:"id"`
	Os   string `json:"os"`
	Arch string `json:"arch"`
}

func (b *Build) FillOS(db *gorm.DB) {
	db.First(&b.Os, b.OsID)
}

func (f *Build) SaveFile(uploadFolder string, srcFileName string, file io.Reader) error {
	slug := f.buildPath()
	path := uploadFolder + slug
	err := os.MkdirAll(path, 0755)
	if err != nil {
		return err
	}

	// Выполним 10 попыток создания файла с уникальным именем длинной в 10 символов
	var iterations int
	for iterations := 0; iterations < 10; iterations++ {
		f.FileName = randString(10) + filepath.Ext(srcFileName)
		// проверим существует ли файл с таким же названием
		if _, err := os.Stat(path + f.FileName); err != nil {
			break
		}
	}

	// если количество итераций равно 10, то файл не был создан
	if iterations == 10 {
		return fmt.Errorf("duplicate filename")
	}

	f.Path = slug + f.FileName
	out, err := os.Create(path + f.FileName)
	if err != nil {
		return err
	}
	defer out.Close()
	_, err = io.Copy(out, file)

	return err
}

func (f *Build) buildPath() string {

	now := time.Now()

	slug := strconv.Itoa(now.Year()) + string(filepath.Separator)
	slug += strconv.Itoa(int(now.Month())) + string(filepath.Separator)
	return slug
}

func (f *Build) exists() bool {
	return exists(f.Path)
}

func exists(path string) bool {
	_, err := os.Stat(path)
	if err == nil {
		return true
	}
	if os.IsNotExist(err) {
		return false
	}

	return true
}

func randString(n int) string {
	const (
		letterBytes   = "abcdefghijklmnopqrstuvwxyz"
		letterIdxBits = 6                    // 6 bits to represent a letter index
		letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
		letterIdxMax  = 63 / letterIdxBits   // # of letter indices fitting in 63 bits
	)
	var src = rand.NewSource(time.Now().UnixNano())
	b := make([]byte, n)
	// A src.Int63() generates 63 random bits, enough for letterIdxMax characters!
	for i, cache, remain := n-1, src.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = src.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(letterBytes) {
			b[i] = letterBytes[idx]
			i--
		}
		cache >>= letterIdxBits
		remain--
	}

	return string(b)
}
