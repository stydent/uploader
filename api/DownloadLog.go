package api

import "time"

type DownloadLog struct {
	ID        int       `json:"-"`
	BuildID   int       `json:"-" sql:"index;not null"`
	UserID    int       `json:"-" sql:"index;not null"`
	CreatedAt time.Time `json:"date"`
}
