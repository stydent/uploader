package api

import (
	"fmt"
	"time"

	"github.com/jinzhu/gorm"
)

type Soft struct {
	ID          int       `json:"id"`
	UserID      int       `json:"-" sql:"index"`
	Author      User      `json:"provider,omitempty" sql:"-"`
	CategoryID  int       `json:"-" sql:"index"`
	Category    Category  `json:"cat,omitempty" sql:"-"`
	Name        string    `json:"name" binding:"required" sql:"unique"`
	Description string    `json:"description" binding:"required" gorm:"size:2048"`
	Updates     []Update  `json:"updates,omitempty"`
	CreatedAt   time.Time `json:"created_at"`
	Followers   []User    `gorm:"many2many:follower_soft;" json:"follower_soft,omitempty"`
	CountLikes  int       `json:"count_likes" sql:"DEFAULT:0"`
	LastUpdate  Update    `sql:"-" json:"last_update,omitempty"`
	//LatestVersion string `sql:"-" json:"latest_version,omitempty"`
}
type Update struct {
	ID          int     `json:"id"`
	Description string  `json:"description" gorm:"size:1024"`
	Builds      []Build `json:"builds,omitempty"`
	SoftID      int     `sql:"index" json:"-"`
	Version     string  `sql:"not null" json:"version" form:"version"`
}

func (s *Soft) Create(db *gorm.DB) error {
	if err := db.Create(s).Error; err != nil {
		return err
	}
	return nil
}

func (s *Soft) Save(db *gorm.DB) error {
	if err := db.Save(s).Error; err != nil {
		return err
	}
	return nil
}
func (s *Soft) GetByPK(db *gorm.DB, pk int) error {
	if db.First(s, pk).RecordNotFound() {
		return fmt.Errorf("не найден софт с ID=%d", pk)
	}
	return nil
}

func (s *Soft) FillProvider(db *gorm.DB) {
	db.First(&s.Author, s.UserID)
}
func (s *Soft) FillCategory(db *gorm.DB) {
	db.First(&s.Category, s.CategoryID)
}
func (s *Soft) IncLikes(db *gorm.DB) {
	s.CountLikes++
	db.Save(s)
}
func (s *Soft) FillUpdates(db *gorm.DB) {
	db.Model(&s).Related(&s.Updates)
}

func (s *Soft) FillLastUpdate(db *gorm.DB) {
	if db.Order("id desc").Where(Update{SoftID: s.ID}).First(&s.LastUpdate).RecordNotFound() != false {
		s.LastUpdate.FillBuilds(db)
	}

}

func (u *Update) FillBuilds(db *gorm.DB) {
	db.Model(u).Related(&u.Builds).Find(&u.Builds)
	if u.Builds != nil {
		for idx := range u.Builds {
			u.Builds[idx].FillOS(db)
		}
	}
}
