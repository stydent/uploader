package api

import "time"

type Like struct {
	UserID    int       `json:"id_user" gorm:"index"`
	SoftID    int       `json:"id_soft" gorm:"index"`
	CreatedAt time.Time `json:"created_at"`
}
