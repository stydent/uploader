package api

type AppError struct {
	Message string `json:"message"`
	Code    int    `json:"code"`
}

var (
	ErrAppForbidden      = &AppError{Message: "запрещено", Code: 403}
	ErrAppBinding        = &AppError{Message: "не все поля корректно заполнены", Code: 422}
	ErrAppCantCreate     = &AppError{Message: "не могу создать", Code: 500}
	ErrAppNotFound       = &AppError{Message: "не могу найти", Code: 404}
	ErrAppSessionExpired = &AppError{Message: "сессия истекла", Code: 418}
	ErrAppUnknown        = &AppError{Message: "неизвестная ошибка", Code: 500}
)

func (e *AppError) Error() string {
	return e.Message
}

func NewAppError(message string, code int) *AppError {
	return &AppError{
		Message: message,
		Code:    code,
	}
}
