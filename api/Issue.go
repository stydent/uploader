package api

import "time"

type Issue struct {
	ID        int       `json:"id"`
	UserID    int       `sql:"index" json:"-"`
	Message   string    `json:"message" binding:"required"`
	UpdateID  int       `sql:"index" json:"-"`
	CreatedAt time.Time `json:"created_at"`
}
