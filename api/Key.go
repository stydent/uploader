package api

import (
	"time"

	"github.com/jinzhu/gorm"
)

type Key struct {
	ID        int       `json:"id"`
	UserID    int       `json:"-" sql:"index"`
	User      User      `json:"user,omitempty"`
	SoftID    int       `json:"-" sql:"index"`
	Soft      Soft      `json:"soft,omitempty"`
	Active    bool      `json:"active"`
	Token     string    `json:"key" sql:"unique"`
	CreatedAt time.Time `json:"created_at"`
}

func (k *Key) FillSoft(db *gorm.DB) {
	db.First(&k.Soft, k.SoftID)
}
func (k *Key) FillUser(db *gorm.DB) {
	db.First(&k.User, k.UserID)
}
