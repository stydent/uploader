package api

import (
	"fmt"

	"github.com/jinzhu/gorm"
)

type Category struct {
	ID          int    `json:"id"`
	Name        string `json:"name" sql:"unique"` // TODO:binding:required портит создание софта
	Description string `json:"description" gorm:"size:2048"`
	Softs       []Soft `json:"soft_list,omitempty"`
}

func GetCategoryByPK(db *gorm.DB, pk int) (*Category, error) {
	var c Category
	if db.First(&c, pk).RecordNotFound() {
		return nil, fmt.Errorf("не найдена категория с ID=%d", pk)
	}
	return &c, nil
}

func GetCategoryList(db *gorm.DB) ([]Category, error) {
	var cats []Category

	if err := db.Find(&cats).Error; err != nil {
		return nil, err
	}
	return cats, nil
}

func (c *Category) Create(db *gorm.DB) error {
	if err := db.Create(c).Error; err != nil {
		return err
	}
	return nil
}

func (c *Category) Save(db *gorm.DB) error {
	if err := db.Save(c).Error; err != nil {
		return err
	}
	return nil
}

func (cat *Category) FillSoftList(db *gorm.DB) {
	db.Model(cat).Related(&cat.Softs)
	for idx := range cat.Softs {
		cat.Softs[idx].FillProvider(db)
		cat.Softs[idx].Category = Category{ID: cat.ID, Name: cat.Name, Description: cat.Description}
	}
}
