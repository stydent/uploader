package api

type Permission struct {
	ID          int
	Description string
}
