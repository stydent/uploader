package api

import (
	"bytes"
	"crypto/rand"
	"crypto/sha1"
	"fmt"
	"io"
	"log"
	"time"

	"github.com/jinzhu/gorm"

	"golang.org/x/crypto/pbkdf2"
)

type LoginUser struct {
	Email string `form:"email" json:"email" binding:"required"`
	Pass  string `form:"pass" json:"pass" binding:"required"`
	Name  string `form:"name" json:"name"`
}

type User struct {
	ID           int          `json:"id"`
	Email        string       `json:"email" sql:"not null;unique_index;type:varchar(255)"`
	Name         string       `json:"name"`
	Hash         []byte       `json:"-"`
	Salt         []byte       `json:"-"`
	CreatedAt    time.Time    `json:"created_at"`
	CreatedSoft  []Soft       `json:"created_soft,omitempty"`
	FollowerSoft []Soft       `gorm:"many2many:follower_soft;" json:"follower_soft,omitempty"`
	RoleID       int          `json:"-" sql:"index;not null"`
	Role         Role         `json:"role,omitempty"`
	Permissions  []Permission `json:"permissions,omitempty" sql:"-"`
	Status       int          `json:"status" sql:"DEFAULT:1"`
}

func GetUserByPK(db *gorm.DB, pk int) (*User, error) {
	var u User
	if db.First(&u, pk).RecordNotFound() {
		return nil, fmt.Errorf("не найден пользователь с ID=%d", pk)
	}
	return &u, nil
}

func GetUserList(db *gorm.DB) ([]User, error) {
	var users []User

	if err := db.Find(&users).Error; err != nil {
		return nil, err
	}
	for idx := range users {
		users[idx].FillRole(db)
	}
	return users, nil
}

func (u *User) FillRole(db *gorm.DB) {
	db.First(&u.Role, u.RoleID)
}
func (u *User) FillCreatedSoft(db *gorm.DB) {
	db.Model(u).Related(&u.CreatedSoft)
}

func (user *User) SetPassword(password string) {
	salt := make([]byte, 32)
	_, err := io.ReadFull(rand.Reader, salt)
	if err != nil {
		log.Print(err)
		return
	}
	hash := pbkdf2.Key([]byte(password), salt, 4096, 32, sha1.New)
	user.Salt = salt
	user.Hash = hash
}

func (user *User) IsValidPassword(password string) bool {
	hash := pbkdf2.Key([]byte(password), user.Salt, 4096, 32, sha1.New)
	return bytes.Equal(user.Hash, hash)
}
