package api

type Role struct {
	ID          int          `json:"id"`
	Name        string       `json:"name"`
	Permissions []Permission `gorm:"many2many:role_permission;" json:"-"`
}

func (r *Role) HavePermission(permission string) bool {
	for _, p := range r.Permissions {
		if p.Description == permission {
			return true
		}
	}

	return false
}
