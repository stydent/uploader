package main

import (
	"errors"
	"io/ioutil"
	"log"
	"os"
	"uploader/service"

	"github.com/codegangsta/cli"
	"gopkg.in/yaml.v1"
)

func getConfig(c *cli.Context) (service.Config, error) {
	yamlPath := c.GlobalString("config")
	return getConfigByPath(yamlPath)
}

func getConfigByPath(path string) (service.Config, error) {
	config := service.Config{}

	if _, err := os.Stat(path); err != nil {
		return config, errors.New("config file not found")
	}

	yamlData, err := ioutil.ReadFile(path)
	if err != nil {
		return config, err
	}

	err = yaml.Unmarshal([]byte(yamlData), &config)
	return config, err
}

func main() {
	app := cli.NewApp()
	app.Name = "SoftUpdater"
	app.Version = "0.0.1"

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:  "config",
			Value: "config.yaml",
			Usage: "--config config.yaml\t",
		},
	}

	app.Commands = []cli.Command{
		{
			Name:  "server",
			Usage: "Run the http server",
			Action: func(c *cli.Context) {
				cfg, err := getConfig(c)
				if err != nil {
					log.Fatal(err)
					return
				}

				svc := service.UploaderService{}

				if err = svc.Run(cfg); err != nil {
					log.Fatal(err)
				}
			},
		},
		{
			Name:  "init",
			Usage: "Create admin user, groups, init permissions",
			Action: func(c *cli.Context) {
				cfg, err := getConfig(c)
				if err != nil {
					log.Fatal(err)
					return
				}

				svc := service.UploaderService{}

				if err = svc.Init(cfg); err != nil {
					log.Fatal(err)
				}
			},
		},
		{
			Name:  "migratedb",
			Usage: "Perform database migrations",
			Action: func(c *cli.Context) {
				cfg, err := getConfig(c)
				if err != nil {
					log.Fatal(err)
					return
				}

				svc := service.UploaderService{}

				if err = svc.Migrate(cfg); err != nil {
					log.Fatal(err)
				}
			},
		},
	}
	app.Run(os.Args)

}
