# Серверная часть продукта по обновлению программного обеспечения у клиентов


## Установка проекта

1. Должно быть установлено:
  - git
  - Go
  - Mysql СУБД  
2. Создать БД с именем soft_updater
3. Перейти в дирректорию `cd %GOPATH%\src`
4. Клонировать репозиторий `git clone git@gitlab.com:stydent/uploader.git`
5. Проверить настройки в файле config.yaml
6. Перейти `cd uploader\cmd\server`
7. Выполнить `go get`, затем `go build`
8. Выполнить миграцию бд путем `server.exe --config ../../config.yaml migratedb`
9. Создать администратора и группы `server.exe --config ../../config.yaml init`
10. Запуск сервера командой `server.exe --config ../../config.yaml server`

## Использование

При отправке запросов использовался cURL

### Создание пользователя
Создавать можно только поьзователя в группе Clients. Изменение пользователя и повышение привелегий доступны только Administrators

`curl -i --header "Content-Type:application/json" \
--request POST --data "{\"email\":\"user@user.us\", \"pass\":\"iamstupiduser\"}" \
http://localhost:8080/register`

### Авторизация и получение токена

`curl -i --header "Content-Type:application/json" \
 --request POST --data "{\"email\":\"admin@admin.adm\", \"pass\":\"123\"}" \
 http://localhost:8080/login`

### Создание Программного продукта
Здесь и далее в заголовке `X-Auth` прописывается полученный в предудыщем пункте токен

`curl -i --header "Content-Type:application/json" --request POST \
--header "X-Auth: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE0NTYzMjQzODQsImlkX3VzZXIiOjF9.lWgroZeKa733NV_Lf7Rk2eTG6ZSrfbPGWfERAZJL8E0" \
--data "{\"name\":\"test\", \"description\":\"123\"}" \
http://localhost:8080/soft`

### Добавление обновления
В примере в качестве файла выбран update.zip, который должен лежать в дирректории, где запускается cURL

`curl --header "Content-Type:multipart/form-data" --request POST \
--header "X-Auth: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE0NTYzMjQzODQsImlkX3VzZXIiOjF9.lWgroZeKa733NV_Lf7Rk2eTG6ZSrfbPGWfERAZJL8E0" \
-F "description=test description update for soft witn ID 1" -F "upload=@update.zip" \
http://localhost:8080/soft/1/update`

### Подписаться нв обновления
Здесь 1 в URL - это ID ПО, на который требуется подписаться

`curl -i --header "Content-Type:application/json" --request POST \
--header "X-Auth: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE0NTYzMjQzODQsImlkX3VzZXIiOjF9.lWgroZeKa733NV_Lf7Rk2eTG6ZSrfbPGWfERAZJL8E0" \
http://localhost:8080/follow/soft/1`

### Получить список программных продуктов

`curl -i --header "Content-Type:application/json" --request GET \
--header "X-Auth: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE0NTYzMjQzODQsImlkX3VzZXIiOjF9.lWgroZeKa733NV_Lf7Rk2eTG6ZSrfbPGWfERAZJL8E0" \
http://localhost:8080/soft`

### Получить данные о программном продукте по его ID
Здесь 1 в URL - это ID ПО, информацию о котором требуется получить

`curl -i --header "Content-Type:application/json" --request GET \
--header "X-Auth: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE0NTYzMjQzODQsImlkX3VzZXIiOjF9.lWgroZeKa733NV_Lf7Rk2eTG6ZSrfbPGWfERAZJL8E0" \
http://localhost:8080/soft`
