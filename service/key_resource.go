package service

import (
	"fmt"
	"strconv"
	"uploader/api"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"github.com/satori/go.uuid"
)

type KeyResource struct {
	db *gorm.DB
}

func (k *KeyResource) BuildKey(c *gin.Context) {
	session := c.MustGet("session").(*SessionEntry)
	if !session.HavePermission("build_key") {
		c.Error(fmt.Errorf("отклонен запрос на построение ключа ПО пользователем ID=%d", session.ID)).SetMeta(api.ErrAppForbidden).SetType(gin.ErrorTypePublic)
		return
	}

	idUser, _ := strconv.Atoi(c.Param("id_user"))
	idSoft, _ := strconv.Atoi(c.Param("id_soft"))
	var (
		soft api.Soft
		user api.User
	)
	if k.db.First(&soft, idSoft).RecordNotFound() {
		c.Error(fmt.Errorf("не найден софт с ID=%d", idSoft)).SetMeta(api.ErrAppNotFound).SetType(gin.ErrorTypePublic)
		return
	}
	if k.db.First(&user, idUser).RecordNotFound() {
		c.Error(fmt.Errorf("не найден пользователь с ID=%d", idUser)).SetMeta(api.ErrAppNotFound).SetType(gin.ErrorTypePublic)
		return
	}
	var key api.Key
	if k.db.Where(api.Key{UserID: idUser, SoftID: idSoft}).First(&key).RecordNotFound() {
		token := uuid.NewV4()
		key = api.Key{
			UserID: idUser,
			SoftID: idSoft,
			Token:  token.String(),
			Active: true,
		}
		err := k.db.Create(&key).Error
		if err != nil {
			c.Error(err).SetMeta(api.ErrAppBinding).SetType(gin.ErrorTypePublic)
			return
		}
		c.Set("code", 201)
		c.Set("data", &key)
	} else {
		c.Error(fmt.Errorf("дубль значения ключа")).SetMeta(api.ErrAppBinding).SetType(gin.ErrorTypePublic)
	}
}

func (k *KeyResource) GetUserKeys(c *gin.Context) {
	session := c.MustGet("session").(*SessionEntry)

	// Планируется просмотр ключей только для тех у кого права и владельцев
	idUser, _ := strconv.Atoi(c.Param("id_user"))
	if !session.HavePermission("read_key") && idUser != session.ID {
		c.Error(fmt.Errorf("отклонен запрос на получение ключей ПО пользователем ID=%d", session.ID)).SetMeta(api.ErrAppForbidden).SetType(gin.ErrorTypePublic)
		return
	}
	var keys []api.Key
	if err := k.db.Where(api.Key{UserID: idUser}).Find(&keys).Error; err != nil {
		c.Error(err).SetMeta(api.ErrAppNotFound).SetType(gin.ErrorTypePublic)
		return
	}
	for idx := range keys {
		keys[idx].FillSoft(k.db)
		keys[idx].FillUser(k.db)
	}
	c.Set("data", &keys)
}

func (k *KeyResource) GetKeys(c *gin.Context) {
	session := c.MustGet("session").(*SessionEntry)
	if !session.HavePermission("read_key") {
		c.Error(fmt.Errorf("отклонен запрос на получение ключей ПО пользователем ID=%d", session.ID)).SetMeta(api.ErrAppForbidden).SetType(gin.ErrorTypePublic)
		return
	}
	var keys []api.Key
	if err := k.db.Find(&keys).Error; err != nil {
		c.Error(err).SetMeta(api.ErrAppNotFound).SetType(gin.ErrorTypePublic)
		return
	}
	for idx := range keys {
		keys[idx].FillSoft(k.db)
		keys[idx].FillUser(k.db)
	}
	c.Set("data", &keys)
}
