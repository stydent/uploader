package service

import (
	"strconv"
	"strings"

	"uploader/api"
	"uploader/middleware"

	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
)

type Config struct {
	SvcHost             string
	DbUser              string
	DbPassword          string
	DbHost              string
	DbName              string
	SecretKey           string
	UploadFolder        string
	AdminUserToCreate   api.LoginUser
	RolesToCreate       map[int]RoleConfig
	PermissionsToCreate map[int]string
	OsToCreate          []string
}
type RoleConfig struct {
	Name string
	Perm string
}

type UploaderService struct{}

func (s *UploaderService) getDb(cfg Config) (*gorm.DB, error) {
	connectionString := cfg.DbUser + ":" + cfg.DbPassword + "@tcp(" + cfg.DbHost + ":3306)/" + cfg.DbName + "?charset=utf8&parseTime=True"

	return gorm.Open("mysql", connectionString)
}

func (s *UploaderService) Migrate(cfg Config) error {
	db, err := s.getDb(cfg)
	if err != nil {
		return err
	}

	db.AutoMigrate(
		&api.Permission{}, &api.Role{},
		&api.User{}, &api.Soft{},
		&api.Update{}, &api.Build{},
		&api.DownloadLog{}, &api.Issue{},
		&api.OS{}, &api.Category{},
		&api.Key{}, &api.Like{},
	)
	return nil
}

func (s *UploaderService) Init(cfg Config) error {
	db, err := s.getDb(cfg)
	if err != nil {
		return err
	}

	// Создание списка возможных действий
	permissions := make(map[int]api.Permission)
	for id, perm := range cfg.PermissionsToCreate {
		permissions[id] = api.Permission{ID: id, Description: perm}
		db.Create(permissions[id])
	}

	// Создание ролей и разрешение действий
	for id, role := range cfg.RolesToCreate {
		perms := strings.Split(role.Perm, ",")
		role := api.Role{ID: id, Name: role.Name}
		for _, p := range perms {
			idPerm, _ := strconv.Atoi(p)
			role.Permissions = append(role.Permissions, permissions[idPerm])
		}
		err = db.Create(&role).Error
		if err != nil {
			return err
		}
	}

	// Регистрация пользователя как админа
	var user api.User
	user.Email = cfg.AdminUserToCreate.Email
	user.RoleID = 1 // TODO: убрать хардкод
	user.SetPassword(cfg.AdminUserToCreate.Pass)
	err = db.Create(&user).Error
	if err != nil {
		return err
	}

	// Созание OS
	for _, oS := range cfg.OsToCreate {
		s := strings.Split(oS, " ")
		o := api.OS{Os: s[0], Arch: s[1]}
		err = db.Create(&o).Error
		if err != nil {
			return err
		}
	}

	return nil
}

func (s *UploaderService) Run(cfg Config) error {
	db, err := s.getDb(cfg)
	if err != nil {
		return err
	}

	// Инициализируем хранилище сессий
	sessionStore, err := GetSessionStore()
	if err != nil {
		return err
	}
	defer sessionStore.Close()

	softResource := &SoftResource{db: db, uploadDir: cfg.UploadFolder, h: &h}
	userResource := &UserResource{db: db, authKey: cfg.SecretKey, session: sessionStore}
	categoryResource := &CategoryResource{db: db}
	keyResource := &KeyResource{db: db}

	go h.run()

	r := gin.New()

	r.Use(middleware.CORS())
	r.Use(gin.Logger())
	r.Use(middleware.PrepareError)
	r.Use(gin.Recovery())
	r.Use(middleware.JSON)

	authGroup := r.Group("/")
	//authGroup.Use(middleware.AuthMiddleware(cfg.SecretKey, db))
	authGroup.Use(sessionStore.Auth)
	{
		authGroup.GET("/soft", softResource.GetSoftList)
		authGroup.POST("/soft", softResource.CreateSoft)
		authGroup.GET("/soft/:id_soft", softResource.GetSoft)
		authGroup.PUT("/soft/:id_soft", softResource.UpdateSoft)
		authGroup.PUT("/soft/:id_soft/like", softResource.SetLikeToSoft)
		authGroup.POST("/soft/:id_soft/update", softResource.CreateUpdate)
		authGroup.GET("/soft/:id_soft/update", softResource.GetUpdateList)
		authGroup.POST("/soft/:id_soft/update/:id_update/os/:id_os/build", softResource.CreateBuild)
		authGroup.POST("/soft/:id_soft/update/:id_update/issue", softResource.CreateIssue)

		authGroup.GET("/category", categoryResource.GetCategoryList)
		authGroup.POST("/category", categoryResource.CreateCategory)
		authGroup.GET("/category/:id_category", categoryResource.GetCategory)
		authGroup.PUT("/category/:id_category", categoryResource.UpdateCategory)
		authGroup.POST("/category/:id_category/soft", softResource.CreateSoftForCategory)

		authGroup.GET("/static/:id_build", softResource.GetStaticFile)

		//authGroup.GET("/follow/soft", softResource.GetSoftList)
		authGroup.POST("/follow/soft/:id_soft", softResource.FollowToSoft)

		authGroup.GET("/users", userResource.GetUserList)
		authGroup.GET("/users/:id_user", userResource.GetUser)
		authGroup.PUT("/users/:id_user", userResource.UpdateUser)
		authGroup.PUT("/users/:id_user/role", userResource.UpgradeClientRole)

		authGroup.GET("/keys", keyResource.GetKeys)
		authGroup.GET("/keys/users/:id_user", keyResource.GetUserKeys)
		authGroup.POST("/keys/users/:id_user/soft/:id_soft", keyResource.BuildKey)

		authGroup.GET("/notifications", serveWs)
	}

	r.POST("/register", userResource.CreateUser)
	r.POST("/login", userResource.LoginUser)

	r.Run(cfg.SvcHost)

	return nil
}
