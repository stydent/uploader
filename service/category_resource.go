package service

import (
	"fmt"
	"strconv"
	"uploader/api"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

type CategoryResource struct {
	db *gorm.DB
}

func (cr *CategoryResource) CreateCategory(c *gin.Context) {
	session := c.MustGet("session").(*SessionEntry)
	if !session.HavePermission("create_category") {
		c.Error(fmt.Errorf("отклонен запрос на создание категории пользователя ID=%d", session.ID)).SetMeta(api.ErrAppForbidden).SetType(gin.ErrorTypePublic)
		return
	}
	var cat api.Category
	if err := c.Bind(&cat); err != nil {
		c.Error(err).SetMeta(api.ErrAppBinding).SetType(gin.ErrorTypePublic)
		return
	}
	err := cat.Create(cr.db)
	if err != nil {
		c.Error(err).SetMeta(api.NewAppError("такое название категории уже занято", 422)).SetType(gin.ErrorTypePublic)
		return
	}
	c.Set("code", 201)
	c.Set("data", &cat)
}

func (cr *CategoryResource) GetCategory(c *gin.Context) {
	session := c.MustGet("session").(*SessionEntry)
	if !session.HavePermission("read_category") {
		c.Error(fmt.Errorf("отклонен запрос на получение информации о категории ID=%s пользователю ID=%d", c.Param("id_category"), session.ID)).SetMeta(api.ErrAppForbidden).SetType(gin.ErrorTypePublic)
		return
	}

	idCategory, err := strconv.Atoi(c.Param("id_category"))
	if err != nil {
		c.Error(err).SetMeta(api.ErrAppNotFound).SetType(gin.ErrorTypePublic)
		return
	}
	cat, err := api.GetCategoryByPK(cr.db, idCategory)
	if err != nil {
		c.Error(err).SetMeta(api.ErrAppNotFound).SetType(gin.ErrorTypePublic)
	} else {
		cat.FillSoftList(cr.db)
		c.Set("data", &cat)
	}
}

func (cr *CategoryResource) GetCategoryList(c *gin.Context) {
	session := c.MustGet("session").(*SessionEntry)
	if !session.HavePermission("read_category") {
		c.Error(fmt.Errorf("отклонен запрос на получение информации о категориях ID=%d", session.ID)).SetMeta(api.ErrAppForbidden).SetType(gin.ErrorTypePublic)
		return
	}

	cats, err := api.GetCategoryList(cr.db)
	if err != nil {
		c.Error(err).SetMeta(api.ErrAppNotFound).SetType(gin.ErrorTypePublic)
	} else {
		c.Set("data", &cats)
	}
}

func (cr *CategoryResource) UpdateCategory(c *gin.Context) {
	session := c.MustGet("session").(*SessionEntry)
	if !session.HavePermission("update_category") {
		c.Error(fmt.Errorf("отклонен запрос на обновление категории пользователя ID=%d", session.ID)).SetMeta(api.ErrAppForbidden).SetType(gin.ErrorTypePublic)
		return
	}

	idCategory, _ := strconv.Atoi(c.Param("id_category"))

	var cat, rCat api.Category
	if cr.db.First(&cat, idCategory).RecordNotFound() {
		c.Error(fmt.Errorf("не найдено категории с ID=%d", idCategory)).SetMeta(api.ErrAppNotFound).SetType(gin.ErrorTypePublic)
		return
	}
	if err := c.Bind(&rCat); err != nil {
		c.Error(err).SetMeta(api.ErrAppBinding).SetType(gin.ErrorTypePublic)
		return
	}

	cat.Name = rCat.Name
	cat.Description = rCat.Description

	err := cat.Save(cr.db)
	if err != nil {
		c.Error(err).SetMeta(api.ErrAppBinding).SetType(gin.ErrorTypePublic)
		return
	}

	c.Set("code", 200)
	c.Set("data", &cat)
}
