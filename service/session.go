package service

import (
	"encoding/json"
	"fmt"
	"time"
	"uploader/api"

	"github.com/garyburd/redigo/redis"
	"github.com/gin-gonic/gin"
	"github.com/satori/go.uuid"
)

const (
	SessionBucket  = "tokens"
	SessionTimeout = 5 * 60
	//SessionCleanupInterval = 2 * time.Minute
)

var SessionExpiredError = fmt.Errorf("Сессия истекла")

type SessionStore struct {
	connection redis.Conn
}

type SessionEntry struct {
	ID          int             `json:"id"`
	Email       string          `json:"email"`
	Name        string          `json:"name"`
	RoleName    string          `json:"role"`
	Permissions map[string]bool `json:"permissions"`
	IssuedAt    int64           `json:"iat"`
	Expires     int64           `json:"exp"`
	XToken      string          `json:"token,omitempty"`
}

func GetSessionStore() (*SessionStore, error) {
	c, err := redis.Dial("tcp", "127.0.0.1:6379")
	if err != nil {
		return nil, err
	}
	//defer c.Close()

	return &SessionStore{connection: c}, nil
}

func (s *SessionEntry) Expired() bool {
	return time.Unix(s.Expires, 0).Before(time.Now())
}
func (s *SessionEntry) HavePermission(permission string) bool {
	allow, ok := s.Permissions[permission]
	return ok && allow
}

func (db *SessionStore) EmitToken(c *gin.Context) {
	user := c.MustGet("user").(*api.User)
	role := c.MustGet("role").(*api.Role)
	now := time.Now()
	key := uuid.NewV4()
	entry := SessionEntry{
		ID:          user.ID,
		Email:       user.Email,
		Name:        user.Name,
		Expires:     now.Add(SessionTimeout).Unix(),
		IssuedAt:    now.Unix(),
		RoleName:    role.Name,
		Permissions: make(map[string]bool),
	}
	for _, p := range role.Permissions {
		entry.Permissions[p.Description] = true
	}
	data, _ := json.Marshal(&entry)

	_, err := db.connection.Do("SET", SessionBucket+":"+key.String(), data)
	if err != nil {
		c.Error(err).SetMeta(api.NewAppError("не могу создать токен", 500)).SetType(gin.ErrorTypePublic)
		return
	}
	db.connection.Do("EXPIRE", SessionBucket+":"+key.String(), SessionTimeout)
	entry.XToken = key.String()
	c.Set("code", 201)
	c.Set("data", &entry)
}

func (db *SessionStore) Close() {
	db.connection.Close()
}
func (db *SessionStore) Auth(c *gin.Context) {
	var entry SessionEntry

	token := c.Request.Header.Get("X-Auth")
	if token == "" {
		token = c.Query("t")
	}
	key, err := uuid.FromString(token)
	if err != nil {
		c.AbortWithError(400, fmt.Errorf("Запрос не содержит валидный токен"))
		return
	}
	reply, err := db.connection.Do("GET", SessionBucket+":"+key.String())
	if reply == nil {
		c.Error(fmt.Errorf("Не найден ключ сессии %s", key.String())).SetMeta(api.ErrAppSessionExpired).SetType(gin.ErrorTypePublic)
		c.Abort()
		return
	}
	if err != nil {
		c.Error(err).SetMeta(api.ErrAppUnknown).SetType(gin.ErrorTypePublic)
		c.Abort()
		return
	}
	if err := json.Unmarshal(reply.([]byte), &entry); err != nil {
		c.Error(err).SetMeta(api.ErrAppUnknown).SetType(gin.ErrorTypePublic)
		c.Abort()
		return
	}
	/*
		if entry.Expired() {
			c.Error(fmt.Errorf("Сессия устарела")).SetMeta(api.ErrAppSessionExpired).SetType(gin.ErrorTypePublic)
			c.Abort()
			return
		}
	*/
	entry.Expires = time.Now().Add(SessionTimeout).Unix()
	data, _ := json.Marshal(&entry)
	_, err = db.connection.Do("SET", SessionBucket+":"+key.String(), data)
	if err != nil {
		c.Error(err).SetMeta(api.ErrAppUnknown).SetType(gin.ErrorTypePublic)
		c.Abort()
		return
	}
	_, err = db.connection.Do("EXPIRE", SessionBucket+":"+key.String(), SessionTimeout)
	if err != nil {
		c.Error(err).SetMeta(api.ErrAppUnknown).SetType(gin.ErrorTypePublic)
		c.Abort()
		return
	}
	c.Set("session", &entry)
	c.Next()
}
