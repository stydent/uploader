package service

import (
	"fmt"
	"strconv"
	"uploader/api"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

type SoftResource struct {
	db        *gorm.DB
	uploadDir string
	h         *hub
}

func (sr *SoftResource) CreateSoft(c *gin.Context) {
	session := c.MustGet("session").(*SessionEntry)
	if !session.HavePermission("create_soft") {
		c.Error(fmt.Errorf("отклонен запрос на создание софта пользователя ID=%d", session.ID)).SetMeta(api.ErrAppForbidden).SetType(gin.ErrorTypePublic)
		return
	}

	var soft api.Soft
	if err := c.Bind(&soft); err != nil {
		c.Error(err).SetMeta(api.ErrAppBinding).SetType(gin.ErrorTypePublic)
		return
	}

	soft.UserID = session.ID
	err := soft.Create(sr.db)
	if err != nil {
		c.Error(err).SetMeta(api.NewAppError("такое название ПО уже занято", 422)).SetType(gin.ErrorTypePublic)
		return
	}
	soft.FillProvider(sr.db)

	c.Set("code", 201)
	c.Set("data", &soft)
}

func (sr *SoftResource) CreateSoftForCategory(c *gin.Context) {
	session := c.MustGet("session").(*SessionEntry)
	if !session.HavePermission("create_soft") {
		c.Error(fmt.Errorf("отклонен запрос на создание софта пользователя ID=%d", session.ID)).SetMeta(api.ErrAppForbidden).SetType(gin.ErrorTypePublic)
		return
	}
	idCategory, _ := strconv.Atoi(c.Param("id_category"))

	var soft api.Soft
	if err := c.BindJSON(&soft); err != nil {
		c.Error(err).SetMeta(api.ErrAppBinding).SetType(gin.ErrorTypePublic)
		return
	}

	if sr.db.First(&soft.Category, idCategory).RecordNotFound() {
		c.Error(fmt.Errorf("не найдена категория с ID=%d", idCategory)).SetMeta(api.ErrAppNotFound).SetType(gin.ErrorTypePublic)
		return
	}

	soft.UserID = session.ID
	soft.CategoryID = idCategory
	err := soft.Create(sr.db)
	if err != nil {
		c.Error(err).SetMeta(api.NewAppError("такое название ПО уже занято", 422)).SetType(gin.ErrorTypePublic)
		return
	}

	c.Set("code", 201)
	c.Set("data", &soft)
}

func (sr *SoftResource) UpdateSoft(c *gin.Context) {
	session := c.MustGet("session").(*SessionEntry)
	if !session.HavePermission("update_soft") {
		c.Error(fmt.Errorf("отклонен запрос на обновление софта пользователя ID=%d", session.ID)).SetMeta(api.ErrAppForbidden).SetType(gin.ErrorTypePublic)
		return
	}

	idSoft, _ := strconv.Atoi(c.Param("id_soft"))

	var soft, rSoft api.Soft
	if sr.db.Where(&api.Soft{ID: idSoft, UserID: session.ID}).First(&soft).RecordNotFound() {
		c.Error(fmt.Errorf("не найдено софта с ID=%d", idSoft)).SetMeta(api.ErrAppNotFound).SetType(gin.ErrorTypePublic)
		return
	}
	if err := c.Bind(&rSoft); err != nil {
		c.Error(err).SetMeta(api.ErrAppBinding).SetType(gin.ErrorTypePublic)
		return
	}
	soft.Name = rSoft.Name
	soft.Description = rSoft.Description

	err := soft.Save(sr.db)
	if err != nil {
		c.Error(err).SetMeta(api.ErrAppBinding).SetType(gin.ErrorTypePublic)
		return
	}

	c.Set("code", 200)
	c.Set("data", &soft)
}

func (sr *SoftResource) CreateUpdate(c *gin.Context) {
	session := c.MustGet("session").(*SessionEntry)
	if !session.HavePermission("create_soft_update") {
		c.Error(fmt.Errorf("отклонен запрос на загрузку обновлений пользователя ID=%d и софта ID=%s", session.ID, c.Param("id_soft"))).SetMeta(api.ErrAppForbidden).SetType(gin.ErrorTypePublic)
		return
	}

	idSoft, _ := strconv.Atoi(c.Param("id_soft"))
	var soft api.Soft
	if sr.db.Where(&api.Soft{ID: idSoft, UserID: session.ID}).First(&soft).RecordNotFound() {
		c.Error(fmt.Errorf("не найдено софта с ID=%d", idSoft)).SetMeta(api.ErrAppNotFound).SetType(gin.ErrorTypePublic)
		return
	}

	var update api.Update
	if err := c.Bind(&update); err != nil {
		c.Error(err).SetMeta(api.ErrAppBinding).SetType(gin.ErrorTypePublic)
		return
	}

	soft.Updates = append(soft.Updates, update)

	sr.db.Save(&soft)

	// Поскольку ссылка на возвращенный объект из БД лежит в массиве у софта,
	// то возьмем ссылку на него, и построим адрес для скачивания
	update = soft.Updates[len(soft.Updates)-1]
	//update.BuildDownloadLink()
	c.Set("code", 201)
	c.Set("data", &update)
}

func (sr *SoftResource) CreateBuild(c *gin.Context) {
	session := c.MustGet("session").(*SessionEntry)
	// TODO: заменить права на create_build
	if !session.HavePermission("create_soft_update") {
		c.Error(fmt.Errorf("отклонен запрос на загрузку сборки пользователем ID=%d для обновления ID=%s", session.ID, c.Param("id_update"))).SetMeta(api.ErrAppForbidden).SetType(gin.ErrorTypePublic)
		return
	}

	idSoft, _ := strconv.Atoi(c.Param("id_soft"))
	idUpdate, _ := strconv.Atoi(c.Param("id_update"))
	idOS, _ := strconv.Atoi(c.Param("id_os"))

	var os api.OS
	if sr.db.First(&os, idOS).RecordNotFound() {
		c.Error(fmt.Errorf("не найдено ОС с ID=%d", idOS)).SetMeta(api.ErrAppNotFound).SetType(gin.ErrorTypePublic)
		return
	}

	var soft api.Soft
	if sr.db.Where(&api.Soft{ID: idSoft, UserID: session.ID}).First(&soft).RecordNotFound() {
		c.Error(fmt.Errorf("не найдено софта с ID=%d", idSoft)).SetMeta(api.ErrAppNotFound).SetType(gin.ErrorTypePublic)
		return
	}

	var update api.Update
	if sr.db.First(&update, idUpdate).RecordNotFound() {
		c.Error(fmt.Errorf("не найдено обновления с ID=%d", idUpdate)).SetMeta(api.ErrAppNotFound).SetType(gin.ErrorTypePublic)
		return
	}

	file, header, err := c.Request.FormFile("upload")
	if err != nil {
		c.Error(err).SetMeta(api.ErrAppBinding).SetType(gin.ErrorTypePublic)
		return
	}

	build := api.Build{UpdateID: idUpdate, OsID: idOS}

	err = build.SaveFile(sr.uploadDir, header.Filename, file)
	if err != nil {
		c.Error(err).SetMeta(api.ErrAppCantCreate).SetType(gin.ErrorTypePublic)
		return
	}

	if err = sr.db.Create(&build).Error; err != nil {
		c.Error(err).SetMeta(api.ErrAppCantCreate).SetType(gin.ErrorTypePublic)
	} else {

		// Уведомляем пользователей, которые подписаны, об обновлении
		var followers []api.User
		sr.db.Model(&soft).Association("Followers").Find(&followers)
		followerIDs := make([]int, len(followers))
		for _, f := range followers {
			followerIDs = append(followerIDs, f.ID)
		}

		// Просим хаб разослать заданным клиентам сообщения
		h.messageForUsers(
			fmt.Sprintf("Обновление для %s.", soft.Name),
			followerIDs,
		)

		c.Set("code", 201)
		c.Set("data", &build)
	}
}

func (sr *SoftResource) CreateIssue(c *gin.Context) {
	session := c.MustGet("session").(*SessionEntry)
	if !session.HavePermission("create_issue") {
		c.Error(fmt.Errorf("отклонен запрос на отправку пользователем ID=%d комментария к обновлению ID=%s", session.ID, c.Param("id_update"))).SetMeta(api.ErrAppForbidden).SetType(gin.ErrorTypePublic)
		return
	}

	idUpdate, err := strconv.Atoi(c.Param("id_update"))
	if err != nil {
		c.Error(err).SetMeta(api.ErrAppNotFound).SetType(gin.ErrorTypePublic)
		return
	}

	var update api.Update
	if sr.db.First(&update, idUpdate).RecordNotFound() {
		c.Error(fmt.Errorf("не найдено обновления с ID=%d", idUpdate)).SetMeta(api.ErrAppNotFound).SetType(gin.ErrorTypePublic)
		return
	}

	var issue api.Issue
	if err := c.Bind(&issue); err != nil {
		c.Error(err).SetMeta(api.ErrAppBinding).SetType(gin.ErrorTypePublic)
		return
	}
	issue.UserID = session.ID
	issue.UpdateID = update.ID
	sr.db.Create(&issue)

	c.Set("code", 201)
	c.Set("data", &issue)
}

func (sr *SoftResource) GetSoft(c *gin.Context) {
	session := c.MustGet("session").(*SessionEntry)
	if !session.HavePermission("read_soft") {
		c.Error(fmt.Errorf("отклонен запрос на получение информации о софте ID=%s пользователю ID=%d", c.Param("id_soft"), session.ID)).SetMeta(api.ErrAppForbidden).SetType(gin.ErrorTypePublic)
		return
	}

	idSoft, err := strconv.Atoi(c.Param("id_soft"))
	if err != nil {
		c.Error(err).SetMeta(api.ErrAppNotFound).SetType(gin.ErrorTypePublic)
		return
	}

	var soft api.Soft
	err = soft.GetByPK(sr.db, idSoft)
	if err != nil {
		c.Error(err).SetMeta(api.ErrAppNotFound).SetType(gin.ErrorTypePublic)
	} else {
		soft.FillProvider(sr.db)
		soft.FillCategory(sr.db)
		soft.FillLastUpdate(sr.db)
		c.Set("data", &soft)
	}
}

func (sr *SoftResource) GetStaticFile(c *gin.Context) {
	session := c.MustGet("session").(*SessionEntry)
	if !session.HavePermission("read_soft_update") {
		c.Error(fmt.Errorf("отклонен запрос на скачиваеие обновления ID=%s пользователем ID=%d", c.Param("id_update"), session.ID)).SetMeta(api.ErrAppForbidden).SetType(gin.ErrorTypePublic)
		return
	}
	idBuild, err := strconv.Atoi(c.Param("id_build"))
	if err != nil {
		c.Error(err).SetMeta(api.ErrAppNotFound).SetType(gin.ErrorTypePublic)
		return
	}
	var build api.Build
	if sr.db.First(&build, idBuild).RecordNotFound() {
		c.Error(fmt.Errorf("не найдено обновления с ID=%d", idBuild)).SetMeta(api.ErrAppNotFound).SetType(gin.ErrorTypePublic)
		return
	}
	download := api.DownloadLog{
		UserID:  session.ID,
		BuildID: build.ID,
	}
	sr.db.Create(&download)
	c.File(sr.uploadDir + build.Path)
}

func (sr *SoftResource) GetSoftList(c *gin.Context) {
	session := c.MustGet("session").(*SessionEntry)
	if !session.HavePermission("read_soft") {
		c.Error(fmt.Errorf("отклонен запрос на получине списка ПО пользователем ID=%d", session.ID)).SetMeta(api.ErrAppForbidden).SetType(gin.ErrorTypePublic)
		return
	}

	var softs []api.Soft

	sr.db.Order("created_at desc").Find(&softs)
	for idx := range softs {
		softs[idx].FillProvider(sr.db)
		softs[idx].FillCategory(sr.db)
	}
	c.Set("data", &softs)
}

func (sr *SoftResource) GetUpdateList(c *gin.Context) {
	session := c.MustGet("session").(*SessionEntry)
	if !session.HavePermission("read_soft_update") {
		c.Error(fmt.Errorf("отклонен запрос на получине списка обновлений пользователем ID=%d", session.ID)).SetMeta(api.ErrAppForbidden).SetType(gin.ErrorTypePublic)
		return
	}
	idSoft, _ := strconv.Atoi(c.Param("id_soft"))

	var updates []api.Update

	if sr.db.Order("id desc").Where(api.Update{SoftID: idSoft}).Find(&updates).RecordNotFound() {
		c.Error(fmt.Errorf("не найдено обновлений для софта ID=%d", idSoft)).SetMeta(api.ErrAppNotFound).SetType(gin.ErrorTypePublic)
		return
	}

	for idx := range updates {
		updates[idx].FillBuilds(sr.db)
	}
	c.Set("data", &updates)
}

func (sr *SoftResource) FollowToSoft(c *gin.Context) {
	session := c.MustGet("session").(*SessionEntry)
	idSoft, err := strconv.Atoi(c.Param("id_soft"))
	if err != nil {
		c.Error(err).SetMeta(api.ErrAppNotFound).SetType(gin.ErrorTypePublic)
		return
	}
	var soft api.Soft
	if sr.db.First(&soft, idSoft).RecordNotFound() {
		c.Error(fmt.Errorf("не найдено По с ID=%d", idSoft)).SetMeta(api.ErrAppNotFound).SetType(gin.ErrorTypePublic)
		return
	}
	sr.db.Model(&api.User{ID: session.ID}).Association("FollowerSoft").Append(&soft)

	c.Set("code", 204)
	c.Set("data", "success")
}

func (sr *SoftResource) GetFollowSoftList(c *gin.Context) {
	session := c.MustGet("session").(*SessionEntry)
	var softList []api.Soft
	sr.db.Model(&api.User{ID: session.ID}).Association("FollowerSoft").Find(&softList)
	//if len(softList) <= 0 {
	//	c.Error(fmt.Errorf("format string")).SetMeta(api.ErrAppNotFound).SetType(gin.ErrorTypePublic)
	//} else {
	c.Set("code", 200)
	c.Set("data", &softList)
	//}
}

func (sr *SoftResource) SetLikeToSoft(c *gin.Context) {
	session := c.MustGet("session").(*SessionEntry)
	idSoft, _ := strconv.Atoi(c.Param("id_soft"))
	var (
		soft api.Soft
		like api.Like
	)
	if sr.db.First(&soft, idSoft).RecordNotFound() {
		c.Error(fmt.Errorf("не найдено ПО с ID=%d", idSoft)).SetMeta(api.ErrAppNotFound).SetType(gin.ErrorTypePublic)
		return
	}
	if sr.db.Where(api.Like{UserID: session.ID, SoftID: idSoft}).First(&like).RecordNotFound() {
		like = api.Like{
			UserID: session.ID,
			SoftID: idSoft,
		}
		err := sr.db.Create(&like).Error
		if err == nil {
			soft.IncLikes(sr.db)
			c.Set("code", 201)
			c.Set("data", &like)
		} else {
			c.Error(err).SetMeta(api.ErrAppCantCreate).SetType(gin.ErrorTypePublic)
		}
	} else {
		c.Error(fmt.Errorf("попытка повторного лайка пользователя ID=%d ПО ID=%d", session.ID, idSoft)).SetMeta(api.ErrAppForbidden).SetType(gin.ErrorTypePublic)
	}
}
