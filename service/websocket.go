package service

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
)

const (
	maxMessageSize = 1024 * 1024
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  maxMessageSize,
	WriteBufferSize: maxMessageSize,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

var h = hub{
	broadcast:  make(chan string),
	register:   make(chan *client),
	unregister: make(chan *client),
	clients:    make(map[*client]bool),
}

type hub struct {
	// Registered clients
	clients map[*client]bool

	// Inbound messages
	broadcast chan string

	// Register requests
	register chan *client

	// Unregister requests
	unregister chan *client
}

func (h *hub) run() {
	for {
		select {
		case c := <-h.register:
			h.clients[c] = true
			c.send <- []byte("Добро пожаловать!")
			/*
			         countNews := len(h.news)
			   			if countNews > 0 {
			   				// последняя новость
			   				c.send <- []byte(h.news[countNews-1].ToString())
			   			} else {
			   				c.send <- []byte("Not News today!")
			   			}
			*/
			break

		case c := <-h.unregister:
			_, ok := h.clients[c]
			if ok {
				delete(h.clients, c)
				close(c.send)
			}
			break

		case m := <-h.broadcast:
			h.broadcastMessage(m)
			break
		}
	}
}

func (h *hub) messageForUsers(message string, usersID []int) {
	for _, userID := range usersID {
		for c := range h.clients {
			if userID == c.ID {
				select {
				case c.send <- []byte(message):
					break

				default:
					close(c.send)
					delete(h.clients, c)
				}
			}
		}
	}
}

func (h *hub) broadcastMessage(message string) {
	for c := range h.clients {
		select {
		case c.send <- []byte(message):
			break

		default:
			close(c.send)
			delete(h.clients, c)
		}
	}
}

type client struct {
	ws   *websocket.Conn
	send chan []byte
	ID   int
}

func serveWs(c *gin.Context) {
	session := c.MustGet("session").(*SessionEntry)
	if c.Request.Method != "GET" {
		http.Error(c.Writer, "Method not allowed", 405)
		return
	}

	ws, err := upgrader.Upgrade(c.Writer, c.Request, nil)
	if err != nil {
		log.Println(err)
		return
	}

	cl := &client{
		send: make(chan []byte, maxMessageSize),
		ws:   ws,
		ID:   session.ID,
	}

	h.register <- cl

	go cl.await()
}

func (c *client) await() {

	defer c.ws.Close()

	for {
		select {
		case message, ok := <-c.send:
			if !ok {
				c.ws.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}
			if err := c.ws.WriteMessage(websocket.TextMessage, message); err != nil {
				return
			}
		}
	}
}
