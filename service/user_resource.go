package service

import (
	"fmt"
	"strconv"
	"uploader/api"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

type UserResource struct {
	db      *gorm.DB
	authKey string
	session *SessionStore
}

func (ur *UserResource) CreateUser(c *gin.Context) {
	var aUser api.LoginUser

	if err := c.Bind(&aUser); err != nil {
		c.Error(err).SetMeta(api.ErrAppBinding).SetType(gin.ErrorTypePublic)
		return
	}

	var user api.User
	user.Email = aUser.Email
	user.Name = aUser.Name
	user.RoleID = 4 // TODO: убрать хардкод
	user.SetPassword(aUser.Pass)

	if err := ur.db.Create(&user).Error; err != nil {
		// TODO: обработать ошибку. Действительно ли не существует.
		//       пока предположим, что если ошибка, то не найдено.
		c.Error(err).SetMeta(api.NewAppError("такой логин уже занят", 401)).SetType(gin.ErrorTypePublic)
		return
	}

	c.Set("code", 201)
	c.Set("data", &user)
}
func (ur *UserResource) LoginUser(c *gin.Context) {
	var aUser api.LoginUser

	if err := c.Bind(&aUser); err != nil {
		c.Error(err).SetMeta(api.ErrAppBinding).SetType(gin.ErrorTypePublic)
		return
	}
	var user api.User
	if ur.db.Where(&api.User{Email: aUser.Email}).First(&user).RecordNotFound() {
		c.Error(fmt.Errorf("невозможно пройти атворизацию. Логина %s не существует", aUser.Email)).SetMeta(api.ErrAppNotFound).SetType(gin.ErrorTypePublic)
		return
	}
	if user.IsValidPassword(aUser.Pass) == false {
		c.Error(fmt.Errorf("невозможно пройти атворизацию. Не верный пароль для логина %s", aUser.Email)).SetMeta(api.ErrAppNotFound).SetType(gin.ErrorTypePublic)
		return
	}
	if user.Status < 0 {
		c.Error(fmt.Errorf("невозможно пройти авторизацию. Пользователь не активен")).SetMeta(api.ErrAppNotFound).SetType(gin.ErrorTypePublic)
		return
	}
	var role api.Role
	ur.db.Model(&user).Related(&role)
	ur.db.Model(&role).Association("Permissions").Find(&role.Permissions)
	c.Set("user", &user)
	c.Set("role", &role)
	// выдадим токен
	ur.session.EmitToken(c)
}

func (ur *UserResource) UpgradeClientRole(c *gin.Context) {
	session := c.MustGet("session").(*SessionEntry)
	if !session.HavePermission("update_user") {
		c.Error(fmt.Errorf("отклонен запрос на повышение прав с аккаунта ID=%d", session.ID)).SetMeta(api.ErrAppForbidden).SetType(gin.ErrorTypePublic)
		return
	}

	idUser, _ := strconv.Atoi(c.Param("id_user"))
	var user api.User
	if ur.db.First(&user, idUser).RecordNotFound() {
		c.Error(fmt.Errorf("не могу изменить роль, ID=%d не найден", idUser)).SetMeta(api.ErrAppNotFound).SetType(gin.ErrorTypePublic)
		return
	}

	var role api.Role
	if err := c.Bind(&role); err != nil {
		c.Error(err).SetMeta(api.ErrAppBinding).SetType(gin.ErrorTypePublic)
		return
	}
	if ur.db.Where(&api.Role{Name: role.Name}).First(&role).RecordNotFound() {
		c.Error(fmt.Errorf("не найдена роль ID=%d", role.ID)).SetMeta(api.ErrAppNotFound).SetType(gin.ErrorTypePublic)
		return
	}
	user.RoleID = role.ID
	ur.db.Save(&user)
	ur.db.Model(&role).Association("Permissions").Find(&user.Permissions)

	c.Set("code", 200)
	c.Set("data", &role)
}

func (ur *UserResource) GetUserList(c *gin.Context) {
	session := c.MustGet("session").(*SessionEntry)
	if !session.HavePermission("read_user") {
		c.Error(fmt.Errorf("отклонен запрос на получение информации о списке пользователей пользователю ID=%d", session.ID)).SetMeta(api.ErrAppForbidden).SetType(gin.ErrorTypePublic)
		return
	}

	users, err := api.GetUserList(ur.db)
	if err != nil {
		c.Error(err).SetMeta(api.ErrAppNotFound).SetType(gin.ErrorTypePublic)
	} else {
		c.Set("data", &users)
	}
}

func (ur *UserResource) GetUser(c *gin.Context) {
	session := c.MustGet("session").(*SessionEntry)
	if !session.HavePermission("read_user") {
		c.Error(fmt.Errorf("отклонен запрос на получение информации пользлователе ID=%d пользователю ID=%d", c.Param("id_user"), session.ID)).SetMeta(api.ErrAppForbidden).SetType(gin.ErrorTypePublic)
		return
	}

	idUser, _ := strconv.Atoi(c.Param("id_user"))
	user, err := api.GetUserByPK(ur.db, idUser)
	if err != nil {
		c.Error(err).SetMeta(api.ErrAppNotFound).SetType(gin.ErrorTypePublic)
	} else {
		user.FillRole(ur.db)
		user.FillCreatedSoft(ur.db)
		c.Set("data", &user)
	}
}

func (ur *UserResource) UpdateUser(c *gin.Context) {
	session := c.MustGet("session").(*SessionEntry)
	if !session.HavePermission("update_user") {
		c.Error(fmt.Errorf("отклонен запрос на изменения пользователя c аккаунта ID=%d", session.ID)).SetMeta(api.ErrAppForbidden).SetType(gin.ErrorTypePublic)
		return
	}

	idUser, _ := strconv.Atoi(c.Param("id_user"))
	var user, rUser api.User
	if ur.db.First(&user, idUser).RecordNotFound() {
		c.Error(fmt.Errorf("не могу изменить пользователя, ID=%d не найден", idUser)).SetMeta(api.ErrAppNotFound).SetType(gin.ErrorTypePublic)
		return
	}
	if err := c.Bind(&rUser); err != nil {
		c.Error(err).SetMeta(api.ErrAppBinding).SetType(gin.ErrorTypePublic)
		return
	}
	if rUser.Email != "" {
		user.Email = rUser.Email
	}
	if rUser.Status != 0 {
		user.Status = rUser.Status
	}
	user.Name = rUser.Name
	ur.db.Save(&user)

	c.Set("code", 200)
	c.Set("data", &user)
}
